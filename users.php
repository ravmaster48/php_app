<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista Użytkowników</title>
    <link href="resources/css/custom.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="users.php">Lista użytkowników</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="gallery.php">Galeria zdjęć</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="container">
    <div class="center">
        <h1>Lista Użytkowników</h1>
    </div>
</div>
<div class="container">
    <table class="table table-hover" aria-describedby="users list">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Imię</th>
            <th scope="col">Nazwisko</th>
            <th scope="col">Numer indeksu</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $filename = 'resources/static/db/users.txt';
        $file = fopen($filename, 'r');
        if ($file) {
            while (($line = fgets($file)) !== false) {
                list($firstName, $lastName, $orderNumber) = explode(',', trim($line));
                echo "<tr>
                        <td>$firstName</td>
                        <td>$lastName</td>
                        <td>$orderNumber</td>
                      </tr>";
            }
            fclose($file);
        } else {
            echo "<tr>
                    <td colspan='3'>Nie można otworzyć pliku</td>
                  </tr>";
        }
        ?>
        </tbody>
    </table>
</div>
<footer>
    <p>&copy; <?php echo "R.W. " . date("Y"); ?></p>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="resources/js/custom.js"></script>
</body>
</html>
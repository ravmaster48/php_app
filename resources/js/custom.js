
function toggleContrast(){
    var toggleButton = document.getElementById('toggle-contrast');
    var isHighContrastEnabled = document.body.classList.toggle('high-contrast');
    localStorage.setItem('highContrastEnabled', isHighContrastEnabled.toString());
}

// // On page load
document.addEventListener('DOMContentLoaded', function() {
    if (localStorage.getItem('highContrastEnabled') === 'true') {
        document.body.classList.add('high-contrast');
    } else {
        document.body.classList.remove('high-contrast');
    }
});

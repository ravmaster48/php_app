<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Php app">
    <title>Strona Główna</title>
    <link href="resources/css/custom.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" aria-label="Navigation">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="users.php">Lista użytkowników</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="gallery.php">Galeria zdjęć</a>
                </li>
            </ul>
        </div>
        <button id="toggle-contrast" onclick="toggleContrast()">Kontrast</button>
    </nav>
</header>

<main>
    <!--todo get current user info-->
    <div class="container">
        <div class="center">
            <div class="card" style="max-width:400px" role="complementary">
                <img class="card-img-top" src="resources/static/img/img_avatar1.png" alt="User image" style="width:100%">
                <div class="card-body">
                    <p class="card-title">Imię: Rafał</p>
                    <p class="card-text">Nazwisko: Więcław</p>
                    <p class="card-text">Numer indeksu: 157891</p>
                </div>
            </div>
        </div>
    </div>
</main>

<footer>
    <p>&copy; <?php echo "R.W. " . date("Y"); ?></p>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="resources/js/custom.js"></script>
</body>
</html>
# Tytuł Projektu: Dynamiczna Aplikacja Webowa

## Opis Projektu

Ten projekt to dynamiczna aplikacja internetowa stworzona przy użyciu PHP i Bootstrap, która zawiera stronę główną, stronę listy użytkowników oraz galerię zdjęć. Jest ona zaprojektowana do prezentacji dynamicznego generowania treści i interaktywności na nowoczesnej stronie internetowej.

### Dla kogo jest ten projekt?

Aplikacja ta jest idealna dla deweloperów i studentów uczących się, jak integrować PHP z HTML i Bootstrapem w celu tworzenia dynamicznych i responsywnych stron internetowych. Jest również odpowiednia dla małych firm lub zespołów, które potrzebują prostej, ale funkcjonalnej strony internetowej do wyświetlania informacji o użytkownikach i dynamicznego pokazywania obrazów z plików serwerowych.

### Funkcje

- **Strona Główna**: Wyświetla podstawowe informacje oraz linki nawigacyjne do innych stron.
- **Strona Lista Użytkowników**: Dynamicznie czyta i wyświetla dane użytkowników z pliku tekstowego w responsywnej tabeli. Jest to przydatne dla paneli administracyjnych, gdzie wymagane jest zarządzanie użytkownikami.
- **Strona Galeria Zdjęć**: Automatycznie zapełnia się obrazami z określonego katalogu, prezentując każdy obraz w karcie Bootstrap z funkcjonalnością modalną dla większych widoków. Przydatne dla portfoliów, galerii zdjęć lub pokazów produktów.

### Stos Technologiczny

- **PHP**: Skrypty po stronie serwera do obsługi dynamicznego generowania treści.
- **Bootstrap 4**: Framework front-endowy do tworzenia responsywnych i przyjaznych dla urządzeń mobilnych stron internetowych.
- **HTML/CSS**: Standardowe technologie do budowy i stylizacji stron internetowych.

## Instalacja i Konfiguracja

Aby uruchomić tę aplikację na swoim lokalnym komputerze lub serwerze, wykonaj następujące kroki:

1. **Sklonuj repozytorium:**
   ```sh
   git clone https://gitlab.com/ravmaster48/php_app.git
2. **Przejdź do katalogu projektu:**
   ```
   cd php_app
3. **Uruchom serwer apache i plik główny strony:**
   ```
   index.php

## Użycie
Przeglądaj stronę internetową, korzystając z linków dostarczonych w pasku nawigacyjnym. Każda strona pobiera dane zgodnie z opisem i prezentuje je w interfejsie webowym:

- **Home**: Zacznij stąd, aby zobaczyć układ i uzyskać dostęp do innych stron.
- **Lista użytkowników**: Zobacz listę użytkowników załadowanych dynamicznie z pliku tekstowego.
- **Galeria zdjęć**: Przeglądaj obrazy załadowane dynamicznie z katalogu obrazów.
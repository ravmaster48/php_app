<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galeria</title>
    <link href="resources/css/custom.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="users.php">Lista użytkowników</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="gallery.php">Galeria zdjęć</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<main>
    <div class="container">
        <div class="center">
            <button id="show-all-images" class="btn btn-primary">Pokaż wszystkie zdjęcia</button>
        </div>
    </div>
    <div class="container">
        <div class="center">
            <div id="image-gallery" class="row">
                <?php
                $images = glob('resources/img/*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                foreach ($images as $image) {
                    $image_name = basename($image);
                    ?>

                    <!-- Image inside card -->
                    <div class="col-md-4 col-sm-6 mb-4">
                        <div class="card">
                            <img src="<?php echo htmlspecialchars($image); ?>"
                                 class="card-img-top"
                                 alt="<?php echo htmlspecialchars($image_name); ?>"
                                 data-toggle="modal"
                                 data-target="#modal<?php echo md5($image); ?>"
                                 style="cursor: pointer;">
                            <div class="card-body">
                                <p class="card-text"><?php echo htmlspecialchars($image_name); ?></p>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="modal<?php echo md5($image); ?>" tabindex="-1" role="dialog"
                         aria-labelledby="modalLabel<?php echo md5($image); ?>" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="modalLabel<?php echo md5($image); ?>"><?php echo htmlspecialchars($image_name); ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo htmlspecialchars($image); ?>"
                                         alt="<?php echo htmlspecialchars($image_name); ?>"
                                         style="width:100%; height: auto;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="center">
            <div id="gallery-one" class="carousel slide" data-ride="carousel">
                <ul class="carousel-indicators">
                    <?php
                    $images = glob('resources/img/*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                    foreach ($images as $index => $image) {
                        echo '<li data-target="#gallery-one" data-slide-to="' . $index . '"' . ($index === 0 ? ' class="active"' : '') . '></li>';
                    }
                    ?>
                </ul>
                <div class="carousel-inner">
                    <?php foreach ($images as $index => $image): ?>
                        <?php
                        $image_name = basename($image); // Extracts the file name including extension
                        $alt_text = pathinfo($image_name, PATHINFO_FILENAME); // Extracts the file name without extension
                        ?>
                        <div class="carousel-item <?php echo ($index === 0 ? 'active' : ''); ?>">
                            <img src="<?php echo htmlspecialchars($image); ?>" alt="<?php echo htmlspecialchars($alt_text); ?>" width="1100" height="500">
                            <div class="carousel-caption">
                                <h3><?php echo htmlspecialchars($alt_text); ?></h3>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <a class="carousel-control-prev" href="#gallery-one" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#gallery-one" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</main>

<footer>
    <p>&copy; <?php echo "R.W. " . date("Y"); ?></p>
</footer>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        const showAllButton = document.getElementById('show-all-images');
        const gallery = document.getElementById('image-gallery');
        const carousel = document.getElementById('gallery-one');

        gallery.classList.add('d-none'); // set display none as initial render

        showAllButton.addEventListener('click', function () {
            // Toggle display
            gallery.classList.toggle('d-none');
            carousel.classList.toggle('d-none');

            // Update button text based on whether the gallery is currently shown
            if (gallery.classList.contains('d-none')) {
                showAllButton.textContent = 'Pokaż wszystkie zdjęcia';
            } else {
                showAllButton.textContent = 'Pokaż slider';
            }
        });
    });
</script>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="resources/js/custom.js"></script>
</body>
</html>